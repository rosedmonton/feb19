#!/usr/bin/env python

import argparse
import rospy
from std_msgs.msg import String

def client_callback(msg):
    global names
    rospy.loginfo(msg.data)
    names.append(msg.data)

if __name__=="__main__":
    rospy.init_node("connection_tester_master")
    pub = rospy.Publisher("/connection_tester/master", String, queue_size=1)
    rospy.Subscriber("/connection_tester/client", String, client_callback)
    
    names = []
    
    r = rospy.Rate(30)
    
    while not rospy.is_shutdown():
        while len(names):
            pub.publish(names.pop(0))
        r.sleep()
    
    
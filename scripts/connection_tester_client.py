#!/usr/bin/env python

import argparse
import rospy
from std_msgs.msg import *

def subscriber_callback(msg):
    global message_received, args
    
    if msg.data == args.name:
        rospy.loginfo("Got message from master: %s" % msg.data)
        message_received = True
        sub.unregister()
    
if __name__=="__main__":
    rospy.init_node("connection_tester_client", anonymous=True)
    parser = argparse.ArgumentParser(description="description")
    parser.add_argument('name', type=str, help='Your name')
    args = parser.parse_args()
    
    message_received = False
    
    sub = rospy.Subscriber("/connection_tester/master", String, subscriber_callback)
    pub = rospy.Publisher("/connection_tester/client", String, queue_size=1)
    rospy.sleep(2)
    
    pub.publish(data=args.name)
    
    r = rospy.Rate(30)
    while not message_received and not rospy.is_shutdown():        
        r.sleep()
        
    if message_received:
        rospy.loginfo("Success")
    